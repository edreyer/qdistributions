import os

indir="/Users/Ets/Documents/Stelzer/Terminal/qdistributions"
outdir="/Users/Ets/Documents/Stelzer/Terminal/qdistributions/plot_new"
muInjs=[0,4,8,12,16]
muHyps=[4,8,12,16]
vars=["q0","qmu"]
titlex={
        "q0":  "q_{0}",
        "qmu": "q_{#mu}",
       }
title={
        0:  "#mu' = 0",
        4:  "#mu' = 4",
        8:  "#mu' = 8",
        12: "#mu' = 12",
        16: "#mu' = 16",
      }
linecolor={
           0:  "2",
           4:  "6",
           8:  "857",
           12: "797",
           16: "819",
          }

for muInj in muInjs:
  for muHyp in muHyps:
    for var in vars:

        if(                  (muInj > 0) and (abs(muInj - muHyp) > 1e-5 ) ): continue
        if( (var=="q0")  and (muInj ==0) and (muHyp<15)                   ): continue

        card="card_%s_muInj%s_muHyp%s.dat" % (var,str(muInj),str(muHyp))

        f=open(outdir + "/" + card,"w+")

        f.write("file: %s/results_renovate.root\n" % indir)
        f.write("name: results\n")
        f.write("var:  %s\n" % var)
        f.write("binning: (150,-5,25)\n")
        f.write("cond:  (muInj==%s) && (muHyp==%s)\n" % (str(muInj),str(muHyp)))
        if(var=="q0"): muHyp = 0
        f.write("title: %s\n" % ("#mu' = " + str(muInj) + " (#mu = " + str(muHyp) + ")" ))
        f.write("titley: # toys\n")
        f.write("titlex: %s\n" % titlex[var])
        f.write("minx: -5.\n")
        f.write("maxx: 25.\n")
        f.write("miny: 0.5\n")
        f.write("maxy: 2000.\n")
        f.write("logy: 0\n")
        f.write("logx: 0\n")
        f.write("linecolor: %s\n" % linecolor[muInj])
        f.write("legendminx: 0.65\n")
        f.write("legendmaxx: 0.95\n")
        f.write("legendminy: 0.60\n")
        f.write("legendmaxy: 0.85\n")
        #f.write("latex:  %s\n" % (label[channel]))
        #f.write("latexx: 0.6\n")
        #f.write("latexy: 0.77\n")
                                
        f.close()
