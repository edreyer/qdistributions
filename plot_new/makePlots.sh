
#q0 comparison plot

#canvasclean card_q0_muInj0_muHyp16.dat card_q0_muInj8_muHyp8.dat card_q0_muInj12_muHyp12.dat card_q0_muInj16_muHyp16.dat

#qmu comparison plots at each muHyp

#canvasclean card_qmu_muInj8_muHyp8.dat card_qmu_muInj0_muHyp8.dat
#canvasclean card_qmu_muInj12_muHyp12.dat card_qmu_muInj0_muHyp12.dat
#canvasclean card_qmu_muInj16_muHyp16.dat card_qmu_muInj0_muHyp16.dat

#q0 p-values illustration
#canvasclean card_q0_muInj0_muHyp16_p.dat card_q0_muInj0_muHyp16.dat card_q0_muInj16_muHyp16_p.dat card_q0_muInj16_muHyp16.dat
canvasclean card_q0_muInj16_muHyp16_p.dat card_q0_muInj16_muHyp16.dat card_q0_muInj0_muHyp16_p.dat card_q0_muInj0_muHyp16.dat

#qmu p-values illustration
#canvasclean card_qmu_muInj16_muHyp16_p.dat card_qmu_muInj16_muHyp16.dat card_qmu_muInj0_muHyp16_p.dat card_qmu_muInj0_muHyp16.dat
